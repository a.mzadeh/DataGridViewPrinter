﻿using System;
using System.Windows.Forms;
using AliMoghaddasZadeh;

namespace DataGridViewPrinter_Example {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void btnFillTable_Click(object sender, EventArgs e) {
            Random objRand = new Random();
            for (int i = 0, rowNo = 0; i < 20; ++i) {
                int number1 = objRand.Next(1000);
                int number2 = objRand.Next(1000);
                this.dgvData.Rows.Add(
                        ++rowNo,
                        number1,
                        number2,
                        number1 * number2
                    );
            }
        }

        private void btnPrint_Click(object sender, EventArgs e) {
            DataGridViewPrinter.PringGrid(
                this,
                this.dgvData,
                false,
                "DataGridView - Example - RightToLeft",
                "Footer",
                30,
                30,
                4,
                new int[] {
                    this.colRowNo.Index,
                    this.colNumber1.Index,
                    this.colNumber2.Index,
                    this.colResult.Index
                },
                new string[] {
                    this.colRowNo.HeaderText,
                    this.colNumber1.HeaderText,
                    this.colNumber2.HeaderText,
                    "Result is: "
                },
                new int[] { 70, 100, 100, 400 },
                new bool[] { true, false, false, false },
                new bool[] { false, true, true, true },
                25,
                25,
                new System.Drawing.Font("tahoma", 11),
                new System.Drawing.Font("tahoma", 9),
                new System.Drawing.Font[] {
                    new System.Drawing.Font("tahoma", 10, System.Drawing.FontStyle.Bold),
                    new System.Drawing.Font("tahoma", 10, System.Drawing.FontStyle.Bold),
                    new System.Drawing.Font("tahoma", 10, System.Drawing.FontStyle.Bold),
                    new System.Drawing.Font("tahoma", 10, System.Drawing.FontStyle.Bold),
                },
                new System.Drawing.Font[] {
                    new System.Drawing.Font("tahoma", 10),
                    new System.Drawing.Font("tahoma", 10),
                    new System.Drawing.Font("tahoma", 10),
                    new System.Drawing.Font("tahoma", 10, System.Drawing.FontStyle.Bold),
                },
                50,
                50,
                50,
                50
            );
        }
    }
}
