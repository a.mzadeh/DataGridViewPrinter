﻿/* --------------------------------------------------------
 * In the name of God
 * DataGridViewPrinter v1.0
 * by: Ali MoghaddasZadeh (a.mzadeh@gmail.com)
 * -------------------------------------------------------- */

using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace AliMoghaddasZadeh {
    class DataGridViewPrinter {

        PrintDocument doc;
        bool landscape;
        int curRow;
        int totalRowsPerPage;
        int lineHeight;
        int titleHeight;
        int footerHeight;
        int columnHeaderHeight;
        int[] columnsWidth;
        int[] columnsLeft;
        int[] dgvColumnsIndex;
        string title;
        string footer;
        string[] columnsTitle;
        Font titleFont;
        Font footerFont;
        Font[] columnsHeaderFont;
        Font[] dataFont;
        DataGridView dgv;
        int paddingLeft;
        int paddingRight;
        int paddingTop;
        int paddingBottom;
        int columnsCount;
        bool[] centerAlign;
        bool[] numericField;

        public static void PringGrid(
                Form parent,
                DataGridView dgv,
                bool landscape,
                string title, string footer, int titleHeight, int footerHeight,
                int columnsCount, int[] dgvColumnsIndex, string[] columnsTitle, int[] columnsWidth, bool[] centerAlign, bool[] numericField,
                int columnHeaderHeight, int lineHeight,
                Font titleFont, Font footerFont, Font[] columnHeaderFont, Font[] dataFont,
                int paddingTop, int paddingRight, int paddingBottom, int paddingLeft
            ) {
            if (titleFont == null) titleFont = new Font("B Lotus", 14, FontStyle.Bold);
            if (footerFont == null) footerFont = new Font("B Lotus", 10, FontStyle.Bold);
            DataGridViewPrinter printer = new DataGridViewPrinter(
                    parent, dgv, landscape,
                    title, footer, titleHeight, footerHeight,
                    columnsCount, dgvColumnsIndex, columnsTitle, columnsWidth, centerAlign, numericField,
                    columnHeaderHeight, lineHeight,
                    titleFont, footerFont, columnHeaderFont, dataFont,
                    paddingTop, paddingRight, paddingBottom, paddingLeft
                );
        }

        private DataGridViewPrinter(
                Form parent,
                DataGridView dgv,
                bool landscape,
                string title, string footer, int titleHeight, int footerHeight,
                int columnsCount, int[] dgvColumnsIndex, string[] columnsTitle, int[] columnsWidth, bool[] centerAlign, bool[] numericField,
                int columnHeaderHeight, int lineHeight,
                Font titleFont, Font footerFont, Font[] columnHeaderFont, Font[] dataFont,
                int paddingTop, int paddingRight, int paddingBottom, int paddingLeft
            ) {
            this.doc = new PrintDocument();
            //----------------------------------------------
            PrintDialog dlg = new PrintDialog();
			dlg.UseEXDialog = true;
            dlg.Document = this.doc;
            if (dlg.ShowDialog(parent) != DialogResult.OK) {
                dlg.Dispose();
                this.doc = null;
                return;
            }
            dlg.Dispose();
            //----------------------------------------------            
            if (landscape) this.doc.DefaultPageSettings.Landscape = true;
            this.landscape = landscape;
            this.dgv = dgv;
            this.columnsWidth = columnsWidth;
            this.titleFont = titleFont;
            this.dataFont = dataFont;

            this.totalRowsPerPage =
                (
                    (!landscape ? doc.DefaultPageSettings.PaperSize.Height : doc.DefaultPageSettings.PaperSize.Width)
                    - paddingTop - paddingBottom - titleHeight - footerHeight
                ) / lineHeight;

            this.lineHeight = lineHeight;
            this.title = title;
            this.columnsTitle = columnsTitle;
            this.titleHeight = titleHeight;
            this.paddingLeft = paddingLeft;
            this.paddingRight = paddingRight + paddingLeft;
            this.columnsCount = columnsCount;
            this.dgvColumnsIndex = dgvColumnsIndex;
            this.centerAlign = centerAlign;
            this.numericField = numericField;
            this.columnsHeaderFont = columnHeaderFont;
            this.footer = footer;
            this.paddingTop = paddingTop;
            this.paddingBottom = paddingBottom;
            this.footerFont = footerFont;
            this.footerHeight = footerHeight;
            this.columnHeaderHeight = columnHeaderHeight;

            this.columnsLeft = new int[columnsCount];
            if (columnsCount > 0) this.columnsLeft[columnsCount - 1] = paddingLeft;
            for (int i = columnsCount - 2; i >= 0; --i) {
                this.columnsLeft[i] = this.columnsLeft[i + 1] + columnsWidth[i + 1];
            }
            //----------------------------------------------
            this.doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
            this.curRow = 0;
            this.doc.Print();
        }

        void doc_PrintPage(object sender, PrintPageEventArgs e) {
            Pen p = new Pen(Color.Black);
            SolidBrush b = new SolidBrush(Color.Black);
            int tmpTop;
            string str;
            int top = this.paddingTop;
            Graphics g = e.Graphics;
            int i, j;
            int width = e.PageSettings.Landscape ? e.PageSettings.PaperSize.Height : e.PageSettings.PaperSize.Width;
            //----------------------------------------------
            str = this.title;
            g.DrawString(str, this.titleFont, b, (width - g.MeasureString(str, this.titleFont).Width) / 2, top - 10);
            top += this.titleHeight;
            g.DrawLine(p, this.paddingLeft, top, width - this.paddingRight, top);

            tmpTop = top + this.columnHeaderHeight;
            g.DrawLine(p, width - this.paddingRight, top, width - this.paddingRight, tmpTop);
            for (i = 0; i < this.columnsCount; ++i) {
                g.DrawString(this.columnsTitle[i], this.columnsHeaderFont[i], b, this.columnsLeft[i] + (this.columnsWidth[i] - g.MeasureString(this.columnsTitle[i], this.columnsHeaderFont[i]).Width) / 2, top);
                g.DrawLine(p, columnsLeft[i], top, columnsLeft[i], tmpTop);
            }
            top = tmpTop;
            g.DrawLine(p, this.paddingLeft, top, width - this.paddingRight, top);

            for (i = 0; this.curRow < this.dgv.Rows.Count && i < this.totalRowsPerPage; ++this.curRow, ++i) {
                tmpTop = top + lineHeight;
                g.DrawLine(p, width - this.paddingRight, top, width - this.paddingRight, tmpTop);

                for (j = this.columnsCount - 1; j >= 0; --j) {
                    if (!this.numericField[j]) {
                        try {
                            str = this.dgv[this.dgvColumnsIndex[j], this.curRow].Value.ToString();
                        } catch {
                            str = "";
                        }
                    } else {
                        try {
                            str = String.Format("{0:n0}", Math.Abs(int.Parse(this.dgv[this.dgvColumnsIndex[j], this.curRow].Value.ToString(), System.Globalization.NumberStyles.Any)));
                        } catch {
                            str = "0";
                        }
                    }
                    if (this.centerAlign[j]) {
                        g.DrawString(str, this.dataFont[j], b, this.columnsLeft[j] + (this.columnsWidth[j] - g.MeasureString(str, this.dataFont[j]).Width) / 2, top);
                    } else {
                        g.DrawString(str, this.dataFont[j], b, this.columnsLeft[j] + (this.columnsWidth[j] - g.MeasureString(str, this.dataFont[j]).Width), top);
                    }
                    g.DrawLine(p, columnsLeft[j], top, columnsLeft[j], tmpTop);
                }

                g.DrawLine(p, this.paddingLeft, tmpTop, width - this.paddingRight, tmpTop);
                top = tmpTop;
            }

            str = this.footer + " - صفحه " + (((this.curRow - 1) / this.totalRowsPerPage) + 1) + " از " + ((this.dgv.Rows.Count / this.totalRowsPerPage) + 1);
            top = (!this.landscape ? e.PageSettings.PaperSize.Height : e.PageSettings.PaperSize.Width) - this.paddingBottom - this.footerHeight + (int)g.MeasureString(str, this.footerFont).Height;
            g.DrawString(str, this.footerFont, b, (width - g.MeasureString(str, this.footerFont).Width) / 2, top);

            e.HasMorePages = this.curRow < this.dgv.Rows.Count - 1;
        }

    }
}
